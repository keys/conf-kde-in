# Akademy Website

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_akademy-kde-org)](https://binary-factory.kde.org/job/Website_akademy-kde-org/)

This is the git repository for <a href="https://akademy.kde.org" rel="me">akademy.kde.org</a>, the website for Akademy.

As a (Hu)Go module, it requires both Hugo and Go to work.

### Development
Read about the shared theme at [hugo-kde wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/).

#### Site structure
- Each year's contents are in a `section` (a directory under `content` that contains a `_index.md` file);
- A `menu` is associated with each section using the year number and is only displayed on pages of that section. Pages that don't belong to any section have a menu named "main";

#### Menu
- If a menu entry links to a page on this site, the entry should be defined in the content file of that page (so that highlighting works correctly);
- Otherwise (when the entry is only to contain sub-entries or when it links to an external page), it must be defined in the menu config file `config/_default/menus.yaml`;
- The "main" menu contains entries of recent years, the oldest of which will be moved into "Previous Years" sub-menu when a new year is added. To make our life a little bit easier in such events:
  - Associate each year with a weight by the formula: `weight = 3000 - year`;
  - When a year should be moved to "Previous Years" sub-menu, we don't need to change any weight and only need to add `parent: prev-years` to the `main` field in the corresponding `_index.md` file of that year.

#### Page structure with sponsor bar
- Pages that belong to a section and don't have `hideSponsors: true` will have a sponsor bar showing sponsors of the year of the section;
- Contents in content files are shown on the left of the bar;
- Data for sponsors of each year are defined in `data` directory: entity ids in data files are used to make hash links to specific parts of the "sponsors" page, so the "sponsors" page needs to have these ids for those links to work.

#### Shortcodes
- `sponsor`: a paired shortcode to show information about a sponsor on the "sponsors" page, `.Inner` will be shown on the left, on the right is an image specified by `img` parameter, containing a link specified by `homepage` parameter.
